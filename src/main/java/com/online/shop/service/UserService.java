package com.online.shop.service;

import com.online.shop.dto.UserDetailsDto;
import com.online.shop.dto.UserDto;
import com.online.shop.entities.User;
import com.online.shop.mapper.UserMapper;
import com.online.shop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserMapper userMapper;


    public void registerUser(UserDto userDto) {
        User user = userMapper.map(userDto);
        userRepository.save(user);
    }

    public UserDetailsDto getUserDetailsDtoByEmail (String email){
        Optional<User> optionalUser = userRepository.findByEmail(email);
        User user = optionalUser.get();
        UserDetailsDto userdetailsDto = new UserDetailsDto();
        userdetailsDto.setFullName(user.getFullName());
        userdetailsDto.setAddress(user.getAddress());
        return userdetailsDto;

    }
}
