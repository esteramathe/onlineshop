package com.online.shop.dto;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ChosenProductDto {

    private String chosenQuantity;
    private ProductDto productDto;

}
